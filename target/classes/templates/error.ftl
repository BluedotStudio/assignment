<!DOCTYPE html>

<html lang="en">
<link rel="stylesheet" href="/css/ele.css">
<body>

<body>
<div id="app">
    <el-button @click="visible = true">Button</el-button>
    <el-dialog :visible.sync="visible" title="Hello world">
        <p>Something went wrong: ${status} ${error}</p>
    </el-dialog>
</div>
</body>
<script type="text/javascript" src="/js/vue.js"></script>
<script type="text/javascript" src="/js/ele.js"></script>
<script type="text/javascript">
    new Vue({
        el: '#app',
        data: function() {
            return { visible: false }
        }
    }
</script>
</html>