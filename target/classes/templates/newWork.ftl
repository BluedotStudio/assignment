<!DOCTYPE html>
<html lang="en">
<head>
    <link rel="stylesheet" href="/libs/element/ele.css">
    <style type="text/css">
        .el-header, .el-footer {
            font-family: "Hiragino Sans GB",
            color: #333;
            text-align: center;
        }


    </style>
</head>
<body>
<div id="app">
    <el-container style="height: 100%">
        <el-header>
            <el-menu
                    :default-active="activeIndex2"
                    class="el-menu-demo"
                    mode="horizontal"
                    @select="handleSelect"
                    background-color="#545c64"
                    text-color="#fff"
                    active-text-color="#ffd04b"
                    unique-opened="true">
                <el-menu-item index="1">首页</el-menu-item>
                <el-menu-item index="2">作业中心</el-menu-item>
                <el-menu-item index="3">
                    <el-badge :value="3" class="item">我的作业</el-badge>
                </el-menu-item>
                <el-menu-item index="4">发布作业</el-menu-item>
                <el-menu-item index="5">人员管理</el-menu-item>
                <el-menu-item index="6" disabled></el-menu-item>
                <el-menu-item index="7" disabled></el-menu-item>
                <el-menu-item index="8" disabled></el-menu-item>
                <el-submenu index="9">
                    <template slot="title">X用户</template>
                    <el-menu-item index="6-1">修改密码</el-menu-item>
                    <el-menu-item index="6-2">我的信息</el-menu-item>
                    <el-menu-item index="6-3">退出</el-menu-item>
                </el-submenu>
            </el-menu>
        </el-header>
        <el-main>
            <style>
            </style>
            <template>
                <h3>发布一个新作业</h3>
                <el-form ref="newWorkForm" :model="newWorkForm" label-width="80px">
                    <el-form-item label="作业名称">
                        <el-input v-model="newWorkForm.name"></el-input>
                    </el-form-item>
                    <el-form-item label="截至时间">
                        <el-date-picker
                                v-model="newWorkForm.deadline"
                                type="date"
                                placeholder="选择日期"
                                :picker-options="deadline">
                        </el-date-picker>
                    </el-form-item>
                    <el-form-item label="作业描述">
                        <el-input
                                type="textarea"
                                :rows="2"
                                placeholder="请输入内容"
                                :autosize="{ minRows: 6, maxRows: 8}"
                                v-model="newWorkForm.description">
                        </el-input>
                    </el-form-item>
                    <el-form-item label="选择班级">
                        <template>
                            <el-transfer
                                    filterable
                                    :filter-method="filterMethod"
                                    filter-placeholder="请输入班级拼音"
                                    v-model="newWorkForm.selectClass"
                                    :data="data2">
                            </el-transfer>
                        </template>
                    </el-form-item>
                    <el-form-item>
                        <el-button type="primary" @click="newWorkSubmit">确认提交</el-button>
                    </el-form-item>
                </el-form>

            </template>
        </el-main>
        <el-footer>Copying right @DaiChao <a href="javascript:void(0)">Contact Me</a></el-footer>
    </el-container>
</div>
</body>
<script type="text/javascript" src="/js/vue.js"></script>
<script type="text/javascript" src="/libs/element/ele.js"></script>
<script type="text/javascript">
    var app = new Vue({
        el: '#app',
        data: {
            activeIndex2: '4',
            data2: [{
                lable:'软件1513',
                key:'软件1513'
            },{
                lable:'软件1510',
                key:'软件1510'
            },{
                lable:'软件1512',
                key:'软件1512'
            },{
                lable:'软件1511',
                key:'软件1511'
            }],
            newWorkForm: {
                name: '',
                description: 'description',
                deadline: '',
                selectClass:[]
            },
            deadline: {
                disabledDate(time) {
                    return time.getTime() < Date.now();
                },
                shortcuts: [{
                    text: '今天',
                    onClick(picker) {
                        picker.$emit('pick', new Date());
                    }
                }, {
                    text: '明天',
                    onClick(picker) {
                        const date = new Date();
                        date.setTime(date.getTime() + 3600 * 1000 * 24);
                        picker.$emit('pick', date);
                    }
                }, {
                    text: '三天后',
                    onClick(picker) {
                        const date = new Date();
                        date.setTime(date.getTime() + 3600 * 1000 * 24 * 3);
                        picker.$emit('pick', date);
                    }
                }, {
                    text: '一周后',
                    onClick(picker) {
                        const date = new Date();
                        date.setTime(date.getTime() + 3600 * 1000 * 24 * 7);
                        picker.$emit('pick', date);
                    }
                }]
            }
        },
        methods: {
            handleSelect(key, keyPath) {
                console.log(key, keyPath);
            },
            filterMethod(query, item) {
                return item.lable.indexOf(query) > -1;
            },
            newWorkSubmit() {
                console.log(this.newWorkForm);
            }
        }
    })
</script>
</html>