<!DOCTYPE html>
<html lang="en">
<head>
    <link rel="stylesheet" href="/libs/element/ele.css">
    <style type="text/css">
        .el-header, .el-footer {
            font-family: "Hiragino Sans GB",
            color: #333;
            text-align: center;
        }


        .el-carousel__item:nth-child(2n) {
            background-color: #99a9bf;
        }

        .el-carousel__item:nth-child(2n+1) {
            background-color: #d3dce6;
        }

        .el-carousel__item h3 {
            color: #475669;
            font-size: 20px;
            opacity: 0.75;
            line-height: 200px;
            margin: 0;
        }

        .el-main {
            text-align: center;
        }
    </style>
</head>
<body>
<div id="app">
    <el-container style="height: 100%">
        <el-header>
            <el-menu
                    :default-active="activeIndex2"
                    class="el-menu-demo"
                    mode="horizontal"
                    @select="handleSelect"
                    background-color="#545c64"
                    text-color="#fff"
                    active-text-color="#ffd04b"
                    unique-opened="true">
                <el-menu-item index="1">首页</el-menu-item>
                <el-menu-item index="2">作业中心</el-menu-item>
                <el-menu-item index="3">我的作业</el-menu-item>
                <el-menu-item index="4">发布作业</el-menu-item>
                <el-menu-item index="5">人员管理</el-menu-item>
                <el-submenu index="6">
                    <template slot="title">X用户</template>
                    <el-menu-item index="6-1">修改密码</el-menu-item>
                    <el-menu-item index="6-2">我的信息</el-menu-item>
                    <el-menu-item index="6-3">退出</el-menu-item>
                </el-submenu>
            </el-menu>
        </el-header>
        <el-main>
            <template>
                <el-carousel :interval="4000" type="card" height="400px">
                    <el-carousel-item v-for="item in 6" :key="item">
                        <h3>{{ item }}</h3>
                    </el-carousel-item>
                </el-carousel>
            </template>
        </el-main>
        <el-footer>Copying right @DaiChao <a href="javascript:void(0)">Contact Me</a></el-footer>
    </el-container>
</div>
</body>
<script type="text/javascript" src="/js/vue.js"></script>
<script type="text/javascript" src="/libs/element/ele.js"></script>
<script type="text/javascript">
    var app = new Vue({
        el: '#app',
        data: {
            activeIndex: '1',
            activeIndex2: '1'
        },
        methods: {
            handleSelect(key, keyPath) {
                console.log(key, keyPath);
            }
        }
    })
</script>
</html>