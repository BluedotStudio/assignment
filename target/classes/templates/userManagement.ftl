<!DOCTYPE html>
<html lang="en">
<head>
    <link rel="stylesheet" href="/libs/element/ele.css">
    <style type="text/css">
        .el-header, .el-footer {
            font-family: "Hiragino Sans GB",
            color: #333;
            text-align: center;
        }

        .el-carousel__item:nth-child(2n) {
            background-color: #99a9bf;
        }

        .el-carousel__item:nth-child(2n+1) {
            background-color: #d3dce6;
        }

        .el-carousel__item h3 {
            color: #475669;
            font-size: 20px;
            opacity: 0.75;
            line-height: 200px;
            margin: 0;
        }

        .el-main {
            text-align: center;
        }
    </style>
</head>
<body>
<div id="app">
    <el-container style="height: 100%">
        <el-header>
            <el-menu
                    :default-active="activeIndex2"
                    class="el-menu-demo"
                    mode="horizontal"
                    @select="handleSelect"
                    background-color="#545c64"
                    text-color="#fff"
                    active-text-color="#ffd04b"
                    unique-opened="true">
                <el-menu-item index="1">首页</el-menu-item>
                <el-menu-item index="2">作业中心</el-menu-item>
                <el-menu-item index="3"><el-badge :value="3" class="item">我的作业</el-badge></el-menu-item>
                <el-menu-item index="4">发布作业</el-menu-item>
                <el-menu-item index="5">人员管理</el-menu-item>
                <el-menu-item index="6" disabled>    </el-menu-item>
                <el-menu-item index="7" disabled>    </el-menu-item>
                <el-menu-item index="8" disabled>    </el-menu-item>
                <el-submenu index="9">
                    <template slot="title">X用户</template>
                    <el-menu-item index="6-1">修改密码</el-menu-item>
                    <el-menu-item index="6-2">我的信息</el-menu-item>
                    <el-menu-item index="6-3">退出</el-menu-item>
                </el-submenu>
            </el-menu>
        </el-header>
        <el-main>
            <style>
                .el-table .warning-row {
                    background: oldlace;
                }

                .el-table .success-row {
                    background: #f0f9eb;
                }
            </style>
            <template>
                <h3>第一次作业</h3>
                <el-progress :text-inside="true" :stroke-width="18" :percentage="70"></el-progress>
                <template>
                    <el-table
                            :data="tableData2"
                            style="width: 100%"
                            :row-class-name="tableRowClassName"
                            :default-sort="{prop: 'id', order: 'ascending'}"
                            align="center">
                        <el-table-column
                                prop="id"
                                sortable
                                label="序号">
                        </el-table-column>
                        <el-table-column
                                prop="schoolId"
                                sortable
                                label="学号">
                        </el-table-column>
                        <el-table-column
                                prop="name"
                                label="名字">
                        </el-table-column>
                        <el-table-column
                                prop="date"
                                sortable
                                label="时间">
                        </el-table-column>
                        <el-table-column
                                prop="assignment"
                                label="作业">
                        </el-table-column>
                        <el-table-column
                                prop="status"
                                label="状态">
                        </el-table-column>
                    </el-table>
                </template>

            </template>
        </el-main>
        <el-footer>Copying right @DaiChao <a href="javascript:void(0)">Contact Me</a></el-footer>
    </el-container>
</div>
</body>
<script type="text/javascript" src="/js/vue.js"></script>
<script type="text/javascript" src="/libs/element/ele.js"></script>
<script type="text/javascript">
    var app = new Vue({
        el: '#app',
        data: {
            activeIndex: '1',
            activeIndex2: '2',
            tableData2: [{
                id: 1,
                schoolId: 1,
                date: '2016-05-02',
                name: '王小虎',
                assignment: '上海市普陀区金沙江路 1518 弄',
                status: "已完成"
            }, {
                id: 13,
                schoolId: 3,
                date: '2016-05-02',
                name: '王小虎',
                assignment: '上海市普陀区金沙江路 1518 弄',
                status: "已完成"
            }, {
                id: 4,
                schoolId: 4,
                date: '2016-05-02',
                name: '王小虎',
                assignment: '上海市普陀区金沙江路 1518 弄',
                status: "未交"
            }, {
                id: 5,
                schoolId: 5,
                date: '2016-05-02',
                name: '王小虎',
                assignment: '上海市普陀区金沙江路 1518 弄',
                status: "超时提交"
            }]
        },
        methods: {
            handleSelect(key, keyPath) {
                console.log(key, keyPath);
            },
            tableRowClassName({row, rowIndex}) {
                if (row.status === "未交") {
                    return 'warning-row';
                } else if (row.status === '超时提交') {
                    return 'success-row';
                }
                return '';
            }
        }
    })
</script>
</html>