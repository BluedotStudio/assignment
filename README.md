# Assignment

#### Introduction
一个收作业系统

#### Tools

1. Idea (安装插件 lombok, free mybatis, p3c, gitee)
2. JDK8
3. maven3.3+
4. vs code 写文档

#### Git Flow Dev

1. Fork 本项目
2. 新建 Feat_xxx 分支
3. 提交代码
4. 新建 Pull Request
