package cn.jxau.software.assignment;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import java.util.Date;

/**
 * @DESCRIPATION 首页
 * @AUTHOR 戴超
 * @DATE 2018/6/18
 **/
@Controller
public class IndexController {

    @Value("${application.message:Hello World}")
    private String message = "Hello World";

    @RequestMapping(value = {"/", "/index"}, method = RequestMethod.GET)
    public String index(ModelMap model) {
        model.put("time", new Date());
        model.put("message", this.message);
        return "index";
    }

    @RequestMapping(value = "/workList" , method = RequestMethod.GET)
    public String getWorkList(ModelMap modelMap) {
        return "workList";
    }

    @RequestMapping(value = "/myWork" , method = RequestMethod.GET)
    public String mywork(ModelMap modelMap) {
        return "myWork";
    }

    @RequestMapping(value = "/newWork" , method = RequestMethod.GET)
    public String newWork(ModelMap modelMap) {
        return "newWork";
    }

    @RequestMapping(value = "/newWouserManagement" , method = RequestMethod.GET)
    public String newWouserManagement(ModelMap modelMap) {
        return "newWouserManagement";
    }
}
