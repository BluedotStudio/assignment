<!DOCTYPE html>
<html lang="en">
<head>
    <link rel="stylesheet" href="/libs/element/ele.css">
    <style type="text/css">
        .el-header, .el-footer {
            font-family: "Hiragino Sans GB",
            color: #333;
            text-align: center;
        }

        .el-carousel__item:nth-child(2n) {
            background-color: #99a9bf;
        }

        .el-carousel__item:nth-child(2n+1) {
            background-color: #d3dce6;
        }

        .el-carousel__item h3 {
            color: #475669;
            font-size: 20px;
            opacity: 0.75;
            line-height: 200px;
            margin: 0;
        }

        .el-main {
            text-align: center;
        }
    </style>
</head>
<body>
<div id="app">
    <el-container style="height: 100%">
        <el-header>
            <el-menu
                    :default-active="activeIndex2"
                    class="el-menu-demo"
                    mode="horizontal"
                    @select="handleSelect"
                    background-color="#545c64"
                    text-color="#fff"
                    active-text-color="#ffd04b"
                    unique-opened="true">
                <el-menu-item index="1">首页</el-menu-item>
                <el-menu-item index="2">作业中心</el-menu-item>
                <el-menu-item index="3">
                    <el-badge :value="3" class="item">我的作业</el-badge>
                </el-menu-item>
                <el-menu-item index="4">发布作业</el-menu-item>
                <el-menu-item index="5">人员管理</el-menu-item>
                <el-submenu index="6" style="float: right; padding: 3px 0">
                    <template slot="title">X用户</template>
                    <el-menu-item index="6-1">修改密码</el-menu-item>
                    <el-menu-item index="6-2">我的信息</el-menu-item>
                    <el-menu-item index="6-3">退出</el-menu-item>
                </el-submenu>
            </el-menu>
        </el-header>
        <el-main>
            <style>

            </style>
            <template>
                <el-card class="box-card">
                    <div slot="header" class="clearfix">
                        <span>作业名称1</span>
                        <span style="float: right; padding: 3px 0">某某老师</span>
                    </div>
                    <div>
                        <template class="description">
                            作业描述
                        </template>
                        <template class="upload" >
                            <el-upload
                                    class="upload-demo"
                                    drag
                                    action="https://jsonplaceholder.typicode.com/posts/"
                                    multiple
                                    :before-upload="beforeAvatarUpload">
                                <i class="el-icon-upload"></i>
                                <div class="el-upload__text">将文件拖到此处，或<em>点击上传</em></div>
                                <div class="el-upload__tip" slot="tip">只能上传jpg/png文件，且不超过500kb</div>
                            </el-upload>
                        </template>
                    </div>
                </el-card>
                <br>
                <el-card class="box-card">
                    <div slot="header" class="clearfix">
                        <span>作业名称2</span>
                        <span style="float: right; padding: 3px 0">某某老师</span>
                    </div>
                    <div>
                        <template class="description">
                            作业描述2
                        </template>
                        <template class="upload" >
                            <el-upload
                                    class="upload-demo"
                                    drag
                                    action="https://jsonplaceholder.typicode.com/posts/"
                                    multiple
                                    :before-upload="beforeAvatarUpload">
                                <i class="el-icon-upload"></i>
                                <div class="el-upload__text">将文件拖到此处，或<em>点击上传</em></div>
                                <div class="el-upload__tip" slot="tip">只能上传jpg/png文件，且不超过500kb</div>
                            </el-upload>
                        </template>
                    </div>
                </el-card>

            </template>
        </el-main>
        <el-footer>Copying right @DaiChao <a href="javascript:void(0)">Contact Me</a></el-footer>
    </el-container>
</div>
</body>
<script type="text/javascript" src="/js/vue.js"></script>
<script type="text/javascript" src="/libs/element/ele.js"></script>
<script type="text/javascript">
    var app = new Vue({
        el: '#app',
        data: {
            activeIndex2: 3
        },
        methods: {
            handleSelect(key, keyPath) {
                console.log(key, keyPath);
            },
            beforeAvatarUpload(file) {
                const isJPG = file.type === 'zip/rar/7z';
                const isLt2M = file.size / 1024 / 1024 < 10;

                if (!isJPG) {
                    this.$message.error('上传头像图片只能是 JPG 格式!');
                }
                if (!isLt2M) {
                    this.$message.error('上传头像图片大小不能超过 10MB!');
                }
                return isJPG && isLt2M;
            }
        }
    })
</script>
</html>